#include "tacho.h"

#include "WProgram.h"

#include <avr/interrupt.h>
#include <avr/io.h>

//-----------------------------
// some Global constants
class Constant
  {
  public:
    enum
      {
      AnalogPin = 3,
      MaxSamples = 8000,
      //TODO set lower threshold to lower value for hysteresis
      LowerThreshold = 700,
      UpperThreshold = 700,
      };
  };

//-----------------------------
// Global variables for this cpp
static struct
  {
    // variable to store the converted value
    unsigned int v;

    // whether the current analog state is high or low
    int state;

    // the number of high to low transitions
    int gCount;
    int samples;
    bool busy;

    void init()
      {
      v = 0;
      state = 1;
      gCount = 0;
      samples = 0;
      busy = false;
      }
  } Global;

// ===========================
//ISR(TIMER2_COMPA_vect)
ISR(TIMER1_COMPA_vect)
  {
  Global.samples++;
  if (Global.samples > Constant::MaxSamples)
    {
    // signal main loop we're done
    Global.busy = false;
    }
  else
    {
    Global.v = analogRead(Constant::AnalogPin);
    if (Global.state == 1)
      {
      if (Global.v <= Constant::LowerThreshold)
        {
        Global.gCount++;
        Global.state = 0;
        }
      }
    else // state == 0
      {
      if (Global.v > Constant::UpperThreshold)
        {
        Global.state = 1;
        }
      }
    }
  }

//===========================
Tacho::Tacho() :
    mLastValue(0)
  {
  }

//===========================
int Tacho::lastValue()
  {
  return mLastValue;
  }

//===========================
// Generating a square wave with a period twice the timeout
//    125uS * 2 = 250uS period
// should see this frequency on oscope:
//    1 / 250 uS = 4000Hz

// calculate number of ticks
// the master clock is 16Mhz
//    1 / 16Mhz = 62.5ns per tick
// the timeout requires this many ticks
//    125uS / 62.5nS = 2000 ticks
// use prescalar of 8
//    2000 / 8 = 250 prescalar ticks
void Tacho::setupEvery125uS()
  {
  // set prescalar to "/8" = 010
  //TCCR2B = _BV(CS21);
  TCCR1B = _BV(CS11);

  // set WGM to CTC mode (010)
  // In this mode Timer2 counts up until it matches OCR2A
  // In this mode Timer1 counts up until it matches OCR1A
  //TCCR2A = _BV(WGM21);
  TCCR1A = _BV(WGM11);

  // These are actual measurements from oscope:
  //OCR2A = 249;
  OCR1A = 249;

  // When the OCR2A register matches the Timer2 count, cause an interrupt
  //TIMSK2 = _BV(OCIE2A);
  // When the OCR1A register matches the Timer1 count, cause an interrupt
  TIMSK1 = _BV(OCIE1A);
  }

//===========================
void Tacho::setup()
  {
  // set up timer to interrupt every 125uS
  setupEvery125uS();
  }

//===========================
int Tacho::measure()
  {
  // interrupts are off at this point

  // kick off the sampling session
  Global.busy = true;
  Global.samples = 0;
  Global.gCount = 0;
  // wait for sampling to finish
  while (Global.busy)
    {
    delay(100);
    }

  // interrupts are off now, so it is safe to calculate
  mLastValue = (Global.gCount * 60) / 4;
  return (mLastValue);
  }
