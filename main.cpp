#include "motor.h"
#include "tacho.h"

#include "WProgram.h"

//===========================
Motor motor1(1);
Tacho tacho;

//===========================
void setup()
  {
  Serial.begin(9600); // set up Serial library at 9600 bps
  Serial.println("Motor test!");

  tacho.setup();

  // initialize the motors
  motor1.setup();
  }

//===========================
void doit(uint8_t rate)
  {
  Serial.print("meas: ");
  motor1.runAt(Motor::forward, rate);
  delay(50);
  Serial.print(" ");
  Serial.print(tacho.measure(), DEC);
  Serial.println();
  }

//===========================
// run the motors forward and back
void loop()
  {
  for (uint8_t i = 1; i < 150; i += 3)
    {
    doit(i);
    }

  motor1.idleFor(1000);
  delay(300);
  Serial.print("meas4: ");
  Serial.print(tacho.measure(), DEC);
  Serial.println();

  Serial.println();
  }

//===========================
int main()
  {
  init();
  setup();
  for (;;)
    {
    loop();
    }
  return 0;
  }
